/*
 *@author: lmangoua
 * date: 11/09/18
 */


import static io.restassured.RestAssured.*;
import config.TestConfig;
import org.junit.Test;

public class ListOfAllBreeds extends TestConfig {

    @Test
    public void listOfAllBreedsTest() {

        given().
                log().
                all().
        when().
                get("breeds/list/all").
        then().
                log().
                all();
    }
}
