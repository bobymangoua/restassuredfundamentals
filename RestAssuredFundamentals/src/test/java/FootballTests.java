/*
 *@author: lmangoua
 * date: 12/09/18
 */

import static io.restassured.RestAssured.*;
import static org.hamcrest.core.IsEqual.equalTo;
import config.TestConfig;
import io.restassured.http.ContentType;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import org.junit.Test;

import java.util.List;

public class FootballTests extends TestConfig {

    @Test
    public void getAllCompetitionsOneSeason() {

        given().
                spec(football_requestSpec).
                queryParam("season", 2016).
        when().
                get("competitions/");
    }

    @Test
    public void getTeamCount_OneComp() {

        given().
                spec(football_requestSpec).
        when().
                get("competitions/426/teams").
        then().
                body("count", equalTo(20));
    }

    @Test
    public void getFirstTeamName() {

        given().
                spec(football_requestSpec).
        when().
                get("competitions/426/teams").
        then().
                body("teams.name[0]", equalTo("Hull City FC"));  //Assert the body of HTTP Response
    }

    @Test
    public void getAllTeamData() {

        String responseBody =
                given().
                        spec(football_requestSpec).
                when().
                        get("competitions/426/teams").asString();

        System.out.println(responseBody);
    }

    @Test
    public void getAllTeamData_DoCheckFirst() {

        Response response =
                given().
                        spec(football_requestSpec).
                when().
                        get("competitions/426/teams").
                then().
                        contentType(ContentType.JSON). //to check we are getting Json results
                        extract().response();

        String jsonResponseAsString = response.asString(); //Same as "getAllTeamData()" but this time it checks first for Json contentType
    }

    @Test
    public void extractHeaders() {

        Response response =
                given().
                        spec(football_requestSpec).
                when().
                        get("competitions/426/teams").
                then().
                        contentType(ContentType.JSON). //to check we are getting Json results
                        extract().response();

        Headers headers = response.getHeaders(); //to get all the Headers

        String contentType = response.getHeader("Content-Type"); //to get all the contentType

        System.out.println(contentType);
    }

    @Test
    public void extractFirstTeamName() {

        String firstTeamName =
                given().
                        spec(football_requestSpec).
                when().
                        get("competitions/426/teams").jsonPath().getString("teams.name[0]");

        System.out.println(firstTeamName);
    }

    @Test
    public void extractAllTeamNames() {

        Response response =
                given().
                        spec(football_requestSpec).
                when().
                        get("competitions/426/teams").
                then().
                        contentType(ContentType.JSON). //to check we are getting Json results
                        extract().response();

        List<String> teamNames = response.path("teams.name"); //to get all the Headers

        for(String teamName: teamNames) {
            System.out.println(teamName);
        }
    }
}
