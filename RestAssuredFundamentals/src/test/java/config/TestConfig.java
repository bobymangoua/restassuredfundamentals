/*
 *@author: lmangoua
 * date: 11/9/18
 */

package config;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import static org.hamcrest.Matchers.lessThan;
import io.restassured.config.RestAssuredConfig;
import org.junit.BeforeClass;

public class TestConfig {

    public static RequestSpecification dogAPI_requestSpec;
    public static RequestSpecification football_requestSpec;
    public static ResponseSpecification dogAPI_responseSpec;

    @BeforeClass
    public static void setup() {

        //RestAssured.proxy("localhost", 8888);

        /*RequestSpec dogAPI*/
        dogAPI_requestSpec = new RequestSpecBuilder().
                setBaseUri("https://dog.ceo").
                setBasePath("/api/").
                addHeader("Content-Type", "application/json"). //set the Header & change json to xml when dealing with xml
                addHeader("Accept", "application/json"). //change json to xml when dealing with xml
                build();

        /*RequestSpec footballAPI*/
        football_requestSpec = new RequestSpecBuilder().
                setBaseUri("http://api.football-data.org").
                setBasePath("/v1/").
                addHeader("X-Auth-Token", "787ddae832da4b5bb033a4f6be8142a3"). //set the Header with the API Token Authenticator
                addHeader("X-Response-Control", "minified"). //To get minimum response metadata
                build();

        RestAssured.requestSpecification = dogAPI_requestSpec; //To set "dogAPI_requestSpec"

        RestAssured.requestSpecification = football_requestSpec; //To set "football_requestSpec"

        /*ResponseSpec*/
        dogAPI_responseSpec = new ResponseSpecBuilder().
                expectStatusCode(200). //To assert the test passed
                expectResponseTime(lessThan(3000L)). //To assert response time is < 3s
                build();

        RestAssured.responseSpecification = dogAPI_responseSpec; //To set "dogAPI_responseSpec"
    }
}
