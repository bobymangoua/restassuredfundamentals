/*
 *@author: lmangoua
 * date: 13/09/18
 */

import static io.restassured.RestAssured.*;
import config.TestConfig;
import io.restassured.response.Response;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GPathJSONTests extends TestConfig {

    @Test
    public void extractMapOfElementsWithFind() { //Find returns one result

        Response response = get("competitions/426/teams");
        Map<String,?> allTeamDataForSingleTeam = response.path
                ("teams.find { it.name == 'Leicester City FC' }"); //".find" is a Groovy command and "it." is a special keyword
        System.out.println(allTeamDataForSingleTeam);
    }

    @Test
    public void extractSingleValueWithFind() {

        Response response = get("teams/66/players");
        String certainPlayer = response.path
                ("players.find { it.jerseyNumber == 20 }.name"); //to find players with jerseyNumber 20

        System.out.println(certainPlayer); //display name of that player
    }

    @Test
    public void extractSingleValueWithHighestNumber() {

        Response response = get("teams/66/players");
        String playerName = response.path
                ("players.max { it.jerseyNumber }.name"); //to find players with highest jerseyNumber
        int playerJerseyNumber = response.path
                ("players.max { it.jerseyNumber }.jerseyNumber"); //to find highest jerseyNumber

        System.out.println(playerName); //display name of that player
        System.out.println(playerJerseyNumber); //display highest jerseyNumber
    }

    @Test
    public void extractSingleValueWithSmallestNumber() {

        Response response = get("teams/66/players");
        String playerName = response.path
                ("players.min { it.jerseyNumber }.name"); //to find players with highest jerseyNumber
        int playerJerseyNumber = response.path
                ("players.min { it.jerseyNumber }.jerseyNumber"); //to find highest jerseyNumber

        System.out.println(playerName); //display name of that player
        System.out.println(playerJerseyNumber); //display highest jerseyNumber
    }

    @Test
    public void extractMultipleValuesAndSumThem() {

        //Example of use of Collect and Sum
        Response response = get("teams/66/players");
        int sumOfJerseys = response.path
                ("players.collect { it.jerseyNumber }.sum()"); //to Sum every jerseyNumber

        System.out.println(sumOfJerseys); //display Sum of jerseyNumber
    }

    @Test
    public void extractListOfValueWithFindAll() { //FindAll returns multiple results

        Response response = get("teams/66/players");
        List<String> playerNames = response.path
                ("players.findAll { it.jerseyNumber > 10 }.name"); //to find players with jerseyNumber > 10

        System.out.println(playerNames);
    }

    @Test
    public void extractMapOfObjectWithFindAndFindAll() { //FindAll returns multiple results and Find returns 1 result

        Response response = get("teams/66/players");
        //We use Map because we want to get all of the players's details, not only names
        Map<String,?> playerCertainPosition = response.path
                ("players.findAll { it.position == \"Left Wing\" }.find { it.nationality == \"France\" }"); //to find players from France who plays at position "Left Wing"

        System.out.println(playerCertainPosition);
    }

    @Test
    public void extractMapOfObjectWithFindAndFindAll2() { //FindAll returns multiple results and Find returns 1 result

        String position = "Left Wing";
        String nationality = "France";

        Response response = get("teams/66/players");
        //We use Map because we want to get all of the players's details, not only names
        Map<String,?> playerCertainPosition = response.path
                ("players.findAll { it.position == '%s' }.find { it.nationality == '%s' }",
                position, nationality); //'%s' meaning the variable is a String

        System.out.println(playerCertainPosition);
    }

    @Test
    public void extractMultiplePlayers() {

        String position = "Left-Back";
        String nationality = "England";

        Response response = get("teams/66/players");
        /*We use ArrayList because we want to get multiple players
         *We take ArrayList of players & each one of the players has a Map of
         * their data(Id, position, name, ...)
         */
        ArrayList<Map<String,?>> allPlayersCertainNation = response.path
                ("players.findAll { it.position == '%s' }.findAll { it.nationality == '%s' }",
                        position, nationality); //'%s' meaning the variable is a String

        System.out.println(allPlayersCertainNation);
    }
}
