/*
 *@author: lmangoua
 * date: 11/09/18
 */

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.hasItems;
import config.TestConfig;
import org.junit.Test;

public class VerifyRetriever extends TestConfig {

    @Test
    public void verifyRetrieverTest() {

        given().
                log().
                ifValidationFails().
        when().
                get("breeds/list/all").
        then().
                assertThat().
                body("message.retriever", hasItems("chesapeake", "curly", "flatcoated", "golden"));
    }
}
