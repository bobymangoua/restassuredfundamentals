import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.matchesXsdInClasspath;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.core.IsEqual.equalTo;
import config.EndPoint;
import static org.hamcrest.CoreMatchers.hasItems;
import config.TestConfig;
import org.junit.Test;

public class MyFirstTest extends TestConfig {

    @Test
    public void myFirstTest() {

        given().
                log().
                all(). //To log every info.
                //ifValidationFails(). //to use it we have to comment all() on top
        when().
                get("breeds/image/random").
        then().
                log().
                all();
    }

    @Test
    public void getRandomImage_ResponseTime() {

        long responseTime =
                given().
                        log().
                        all(). //To log every info.
                        //ifValidationFails(). //to use it we have to comment all() on top
                when().
                        get("breeds/image/random").
                        time(); //To measure Response Time

        System.out.println(responseTime);
    }

    @Test
    public void getRandomImage_AssertResponseTime() {

        given().
                log().
                all(). //To log every info.
                //ifValidationFails(). //to use it we have to comment all() on top
        when().
                get("breeds/image/random").
        then().
                time(lessThan(2000L)); //Assert time is less than 2000 milliseconds(2s)
    }

    @Test
    public void getListOfAllBreeds() {

        when().
                get(EndPoint.GET_ALLBREEDS);
    }

    @Test
    public void verifyRetriever() {

        when().
                get(EndPoint.GET_ALLBREEDS).
        then().
                assertThat().
                body("message.retriever", hasItems("chesapeake", "curly", "flatcoated", "golden"));
    }

    @Test
    public void getListOfSubBreeds() {

        when().
                get(EndPoint.GET_SUBBREEDS);
    }

    @Test
    public void getImageRandom() {

        when().
                get(EndPoint.GET_RANDOMIMAGE);
    }

    //For Testing Purpose Not Run the part below
    @Test
    public void createMixedBreed() {

        String createMixedBreedodyJson = "{\n" +
                "    \"status\": \"success\",\n" +
                "    \"message\": {\n" +
                "        \"mixed\": [\n" +
                "            \"chesapeake\",\n" +
                "        ]\n" +
                "   }\n" +
                "}";

        given().
                body(createMixedBreedodyJson).
        when().
                post(EndPoint.GET_ALLBREEDS).
        then();
    }

    @Test
    public void updateRetriever() {

        String updateRetrieverBodyJson = "{\n" +
                "    \"status\": \"success\",\n" +
                "    \"message\": {\n" +
                "        \"retriever\": [\n" +
                "            \"chesapeake\",\n" +
                "            \"curly\",\n" +
                "            \"updated curly\",\n" +
                "            \"flatcoated\",\n" +
                "            \"golden\"\n" +
                "        ]\n" +
                "   }\n" +
                "}";

        given().
                body(updateRetrieverBodyJson).
        when().
                put("breed/retriever"). //hardcode the path to add "updated curly" breed in the list
        then();
    }

    @Test
    public void deleteRetriever() {

        given().
        when().
                delete("breed/retriever/curly"). //hardcode the path to delete "curly" breed in the list
        then();
    }

    //region <testDogAPI_SerialisationByJSON>
    /*This Test is similar to "createMixedBreed",
    but here it converts Java code into Json*/
    @Test
    public void testDogAPI_SerialisationByJSON() {

//        DogAPI dogAPI = new DogAPI("retriever", "golden");
//
//        given().
//                body(dogAPI).
//        when().
//                post(EndPoint.GET_ALLBREEDS).
//        then();
    }
    //endregion

    //region <testDogAPI_SchemaXML>
    @Test
    public void testDogAPI_SchemaXML() {

//        given().
//                pathParam("retriever", "curly").
//        when().
//                get(EndPoint.GET_ALLBREEDS).
//        then().body(matchesXsdInClasspath("DogAPI.xsd"));
    }
    //endregion

    //region <testDogAPI_SchemaXML>
    @Test
    public void testDogAPI_SchemaJSON() {

//        given().
//                pathParam("retriever", "curly").
//        when().
//                get(EndPoint.GET_ALLBREEDS).
//        then().
//                body(matchesJsonSchemaInClasspath("DogAPI_JsonSchema.json"));
    }
    //endregion
}
