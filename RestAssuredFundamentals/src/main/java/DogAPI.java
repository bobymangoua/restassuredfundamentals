/*
 *@author: lmangoua
 * date: 12/09/18
 */

public class DogAPI {

    private String[] message;

    private String status;

    //Generate Constructors
    public DogAPI(String[] message, String status) {
        this.message = message;
        this.status = status;
    }

    public String[] getMessage ()
    {
        return message;
    }

    public void setMessage (String[] message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", status = "+status+"]";
    }
}
